console.log('Hello world')
//GLobal Objects
// Arrays 
let students = [
	"Tony",
	"Peter",
	"Wanda",
	"Vision",
	"Loki"
	];
console.log(students)

arrNum = [15, 20, 30, 11];
arrNum.forEach(num =>{
	if (num % 5 === 0) {
		console.log(`${num} is divisible by 5`)
	} else {
		console.log(false)
	}

})
	
let divisibleBy5 = arrNum.every(num=>{
	console.log(num)
	return num % 5 === 0;
})
console.log(divisibleBy5)


console.log("hello world")

let students = [
    "Tony",
    "Peter",
    "Wanda",
    "Vision",
    "Loki"
]

// ==========
// =Activity=
// ==========
// 1 How do you create arrays in JS?
let arr = []
// 2 How do you access the first character of an array?
arr[0]
// 3 How do you access the last character of an array?
arr[arr.length-1]
// 4 What array method searches for, and returns the index of a given value in an array? 
// This method returns -1 if given value is not found in the array.
indexOf()
// 5 What array method loops over all elements of an array, performing a user-defined function on each iteration?
forEach()
// 6.   What array method creates a new array with elements obtained from a user-defined function?
map()
// 7.   What array method checks if all its elements satisfy a given condition?
every()
// 8.   What array method checks if at least one of its elements satisfies a given condition?
some()
// 9.   True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
false
// 10.   True or False: array.slice() copies elements from original array and returns these as a new array.
true

// Function Coding:
/* 1. */
function addToEnd(students, name){
    if(typeof(name) !== "string") return "error - can only add strings to an array"
    else students.push(name)
    return students
}

/* 2. 
 */
function addToStart(students, name){
    if(typeof(name) !== "string") return "error - can only add strings to an array"
    else students.unshift(name)
    return students
}

/*3*/
function elementChecker(array,element){
    if(array.length === 0) return "error - array must not be empty"
    (array.indexOf(element) === -1) ? false : true
}


/* 4. */
function checkAllStringsEnding(array,char){
    if(array.length === 0) return "error = array must not be empty"
    if(array.some(elem => typeof(elem) !== "string")) return "error - all array elements must be strings"
    if(char.length !== 1) return "error - 2nd argument must be a single character"
    if(typeof(char) !== "string") return "error - 2nd argument must be of data type string"
    return array.every(elem => elem.endsWith(char))
}

/* 5.*/
function stringLengthSorter(array){
    if(array.some(elem => typeof(elem) !== "string")) return "error - all array elements must be strings"
    else return array.sort((a,b) => a.length - b.length)
}

/* 6.*/
function startsWithCounter(array,char){
    
    let counter = 0

    if(array.length === 0) return "error = array must not be empty"
    if(array.some(elem => typeof(elem) !== "string")) return "error - all array elements must be strings"
    if(char.length !== 1) return "error - 2nd argument must be a single character"
    if(typeof(char) !== "string") return "error - 2nd argument must be of data type string"
    array.map(elem => {
        if(elem.toLowerCase().startsWith(char.toLowerCase())) counter += 1
    })
    return counter

}

/* 7.*/
function likeFinder(array,char){
    if(array.length === 0) return "error = array must not be empty"
    if(array.some(elem => typeof(elem) !== "string")) return "error - all array elements must be strings"
    if(typeof(char) !== "string") return "error - 2nd argument must be of data type string"

    let like = []

    array.forEach(elem => {
        if(elem.startsWith(char)) like.push(elem)
    })

    return like
}

/* 8.*/
function randomPicker(array){
    return array[Math.round(Math.random()*(array.length-1))]
}

